<?php

declare(strict_types=1);

namespace Visma\Crud\Controller\Index;

use Exception;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Visma\Crud\Api\TodoItemRepositoryInterfaceFactory as TodoItemRepositoryFactory;

class Index implements HttpGetActionInterface
{
    /**
     * @var RawFactory $rawFactory
     */
    private RawFactory $rawFactory;

    /**
     * @var TodoItemRepositoryFactory $todoItemRepositoryFactory
     */
    private TodoItemRepositoryFactory $todoItemRepositoryFactory;

    /**
     * Index constructor
     *
     * @param RawFactory $rawFactory
     * @param TodoItemRepositoryFactory $todoItemRepositoryFactory
     */
    public function __construct(
        RawFactory $rawFactory,
        TodoItemRepositoryFactory $todoItemRepositoryFactory
    ) {
        $this->rawFactory = $rawFactory;
        $this->todoItemRepositoryFactory = $todoItemRepositoryFactory;
    }

    /**
     * @return ResponseInterface|Raw|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $result = $this->rawFactory->create();

        try {
            $item = $this->todoItemRepositoryFactory->create()->getById(1);
            $itemText = $item->getItemText();
            $result->setContents($itemText);
        } catch (Exception $exception) {
            throw new NoSuchEntityException(__("Couldn't find entity: %1", $exception->getMessage()));
        }

        return $result;
    }
}
