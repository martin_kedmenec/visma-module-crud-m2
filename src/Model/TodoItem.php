<?php

declare(strict_types=1);

namespace Visma\Crud\Model;

use DateTime;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Setup\Declaration\Schema\Dto\Columns\Timestamp;
use Visma\Crud\Api\Data\TodoItemInterface;

class TodoItem extends AbstractModel implements IdentityInterface, TodoItemInterface
{
    private const CACHE_TAG = 'visma_crud';

    /**
     * @inheritDoc
     * @return array
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return string
     */
    public function getItemText(): string
    {
        return $this->getData(self::ITEM_TEXT);
    }

    /**
     * @param int $entityId
     * @param string $itemText
     * @return TodoItemInterface
     */
    public function setItemText($entityId, $itemText): TodoItemInterface
    {
        return $this->setData($entityId, $itemText);
    }

    /**
     * @return DateTime
     */
    public function getDateCompleted(): DateTime
    {
        return $this->getData(self::DATE_COMPLETED);
    }

    /**
     * @param int $entityId
     * @param Timestamp $dateCompleted
     * @return TodoItemInterface
     */
    public function setDateCompleted($entityId, Timestamp $dateCompleted): TodoItemInterface
    {
        return $this->setData($entityId, $dateCompleted);
    }

    /**
     * @return Timestamp
     */
    public function getCreationTime(): Timestamp
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @param int $entityId
     * @param Timestamp $creationTime
     * @return TodoItemInterface
     */
    public function setCreationTime($entityId, Timestamp $creationTime): TodoItemInterface
    {
        return $this->setData($entityId, $creationTime);
    }

    /**
     * @return Timestamp
     */
    public function getUpdateTime(): Timestamp
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @param int $entityId
     * @param Timestamp $updateTime
     * @return TodoItemInterface
     */
    public function setUpdateTime($entityId, Timestamp $updateTime): TodoItemInterface
    {
        return $this->setData($entityId, $updateTime);
    }

    /**
     * @return int
     */
    public function getIsActive(): int
    {
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * @param int $entityId
     * @return TodoItemInterface
     */
    public function setIsActive($entityId): TodoItemInterface
    {
        return $this->setData($entityId);
    }

    protected function _construct()
    {
        $this->_init(ResourceModel\TodoItem::class);
    }
}
