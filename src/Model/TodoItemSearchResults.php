<?php

namespace Visma\Crud\Model;

use Magento\Framework\Api\SearchResults;
use Visma\Crud\Api\Data\TodoItemSearchResultsInterface;

class TodoItemSearchResults extends SearchResults implements TodoItemSearchResultsInterface
{
}
