<?php

declare(strict_types=1);

namespace Visma\Crud\Model\ResourceModel\TodoItem;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Visma\Crud\Model\ResourceModel\TodoItem as TodoItemResource;
use Visma\Crud\Model\TodoItem as TodoItemModel;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(TodoItemModel::class, TodoItemResource::class);
    }
}
