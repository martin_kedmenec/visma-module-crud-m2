<?php

declare(strict_types=1);

namespace Visma\Crud\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class TodoItem extends AbstractDb
{

    private const MAIN_TABLE = 'visma_crud';

    private const ID_FIELD_NAME = 'entity_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(self::MAIN_TABLE, self::ID_FIELD_NAME);
    }
}
