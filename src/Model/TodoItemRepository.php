<?php

declare(strict_types=1);

namespace Visma\Crud\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterfaceFactory as CollectionProcessorFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Visma\Crud\Api\Data\TodoItemInterface;
use Visma\Crud\Api\Data\TodoItemSearchResultsInterface;
use Visma\Crud\Api\Data\TodoItemSearchResultsInterfaceFactory as TodoItemSearchResultsFactory;
use Visma\Crud\Api\TodoItemRepositoryInterface;
use Visma\Crud\Model\ResourceModel\TodoItem as TodoItemResource;
use Visma\Crud\Model\ResourceModel\TodoItem\CollectionFactory;
use Visma\Crud\Model\TodoItemFactory as TodoItemModelFactory;

class TodoItemRepository implements TodoItemRepositoryInterface
{
    /**
     * @var TodoItemResource $todoItemResource
     */
    private TodoItemResource $todoItemResource;

    /**
     * @var TodoItemModelFactory $todoItemModelFactory
     */
    private TodoItemModelFactory $todoItemModelFactory;

    /**
     * @var TodoItemSearchResultsFactory $todoItemSearchResultsFactory
     */
    private TodoItemSearchResultsFactory $todoItemSearchResultsFactory;

    /**
     * @var CollectionFactory $collectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var CollectionProcessorFactory $collectionProcessorFactory
     */
    private CollectionProcessorFactory $collectionProcessorFactory;

    /**
     * TodoItemRepository constructor
     *
     * @param TodoItemResource $todoItemResource
     * @param TodoItemModelFactory $todoItemModelFactory
     * @param TodoItemSearchResultsFactory $todoItemSearchResultsFactory
     * @param CollectionFactory $collectionFactory
     * @param CollectionProcessorFactory $collectionProcessorFactory
     */
    public function __construct(
        TodoItemResource $todoItemResource,
        TodoItemModelFactory $todoItemModelFactory,
        TodoItemSearchResultsFactory $todoItemSearchResultsFactory,
        CollectionFactory $collectionFactory,
        CollectionProcessorFactory $collectionProcessorFactory
    ) {
        $this->todoItemResource = $todoItemResource;
        $this->todoItemModelFactory = $todoItemModelFactory;
        $this->todoItemSearchResultsFactory = $todoItemSearchResultsFactory;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessorFactory = $collectionProcessorFactory;
    }

    /**
     * @param TodoItemInterface $todoItem
     * @return TodoItemInterface
     * @throws CouldNotSaveException
     */
    public function save(TodoItemInterface $todoItem): TodoItemInterface
    {
        try {
            $this->todoItemResource->save($todoItem);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__('Could not save the item: %1', $exception->getMessage()));
        }

        return $todoItem;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return TodoItemSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): TodoItemSearchResultsInterface
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessorFactory->create()->process($searchCriteria, $collection);
        $searchResults = $this->todoItemSearchResultsFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * @param int $todoItemId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws LocalizedException
     */
    public function deleteById($todoItemId): bool
    {
        return $this->delete($this->getById($todoItemId));
    }

    /**
     * @param TodoItemInterface $todoItem
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(TodoItemInterface $todoItem): bool
    {
        try {
            $this->todoItemResource->delete($todoItem);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the item: %1', $exception->getMessage())
            );
        }

        return true;
    }

    /**
     * @param int $todoItemId
     * @return TodoItemInterface
     * @throws NoSuchEntityException
     */
    public function getById($todoItemId): TodoItemInterface
    {
        try {
            $object = $this->todoItemModelFactory->create();
            $this->todoItemResource->load($object, $todoItemId);
        } catch (Exception $exception) {
            throw new NoSuchEntityException(__('Could not get the item: %1', $exception->getMessage()));
        }

        return $object;
    }
}
