<?php
/** @noinspection PhpMissingParamTypeInspection */
/** @noinspection PhpMissingReturnTypeInspection */
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */

declare(strict_types=1);

namespace Visma\Crud\Api\Data;

/**
 * To do item CRUD interface
 * @api
 */
interface TodoItemInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const ITEM_TEXT = 'item_text';

    const DATE_COMPLETED = 'date_completed';

    const CREATION_TIME = 'creation_time';

    const UPDATE_TIME = 'update_time';

    const IS_ACTIVE = 'is_active';

    /**
     * @return string|null
     */
    public function getItemText();

    /**
     * @param int $entityId
     * @param string $itemText
     * @return \Visma\Crud\Api\Data\TodoItemInterface
     */
    public function setItemText($entityId, $itemText);

    /**
     * @return \DateTime|null
     */
    public function getDateCompleted();

    /**
     * @param int $entityId
     * @param \Magento\Framework\Setup\Declaration\Schema\Dto\Columns\Timestamp $dateCompleted
     * @return \Visma\Crud\Api\Data\TodoItemInterface
     */
    public function setDateCompleted(
        $entityId,
        \Magento\Framework\Setup\Declaration\Schema\Dto\Columns\Timestamp $dateCompleted
    );

    /**
     * @return \Magento\Framework\Setup\Declaration\Schema\Dto\Columns\Timestamp|null
     */
    public function getCreationTime();

    /**
     * @param int $entityId
     * @param \Magento\Framework\Setup\Declaration\Schema\Dto\Columns\Timestamp $creationTime
     * @return \Visma\Crud\Api\Data\TodoItemInterface
     */
    public function setCreationTime(
        $entityId,
        \Magento\Framework\Setup\Declaration\Schema\Dto\Columns\Timestamp $creationTime
    );

    /**
     * @return \Magento\Framework\Setup\Declaration\Schema\Dto\Columns\Timestamp|null
     */
    public function getUpdateTime();

    /**
     * @param int $entityId
     * @param \Magento\Framework\Setup\Declaration\Schema\Dto\Columns\Timestamp $updateTime
     * @return \Visma\Crud\Api\Data\TodoItemInterface
     */
    public function setUpdateTime(
        $entityId,
        \Magento\Framework\Setup\Declaration\Schema\Dto\Columns\Timestamp $updateTime
    );

    /**
     * @return int|null
     */
    public function getIsActive();

    /**
     * @param int $entityId
     * @return \Visma\Crud\Api\Data\TodoItemInterface
     */
    public function setIsActive($entityId);
}
