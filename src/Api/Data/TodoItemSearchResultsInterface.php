<?php
/** @noinspection PhpMissingParamTypeInspection */

/** @noinspection PhpMissingReturnTypeInspection */
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */

declare(strict_types=1);

namespace Visma\Crud\Api\Data;

/**
 * To do item CRUD interface
 * @api
 */
interface TodoItemSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get to do items list
     *
     * @return \Visma\Crud\Api\Data\TodoItemInterface[]
     */
    public function getItems();

    /**
     * Set to do items list
     *
     * @param \Visma\Crud\Api\Data\TodoItemInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
