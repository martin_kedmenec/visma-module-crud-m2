<?php
/** @noinspection PhpMissingParamTypeInspection */
/** @noinspection PhpMissingReturnTypeInspection */
/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */

declare(strict_types=1);

namespace Visma\Crud\Api;

/**
 * To do item CRUD interface
 * @api
 */
interface TodoItemRepositoryInterface
{
    /**
     * Save to do item
     *
     * @param \Visma\Crud\Api\Data\TodoItemInterface $todoItem
     * @return \Visma\Crud\Api\Data\TodoItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Visma\Crud\Api\Data\TodoItemInterface $todoItem);

    /**
     * Retrieve to do item
     *
     * @param int $todoItemId
     * @return \Visma\Crud\Api\Data\TodoItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($todoItemId);

    /**
     * Retrieve to do items matching the specified criteria
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Visma\Crud\Api\Data\TodoItemSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete to do item
     *
     * @param \Visma\Crud\Api\Data\TodoItemInterface $todoItem
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Visma\Crud\Api\Data\TodoItemInterface $todoItem);

    /**
     * Delete to do item by ID
     *
     * @param int $todoItemId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($todoItemId);
}
