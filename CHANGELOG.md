# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2021-09-06

### Added

- The module was added.

[0.1.0]: https://@bitbucket.org/martin_kedmenec/module-crud-m2.git
